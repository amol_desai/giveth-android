package com.app.giveth

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    var PACKAGE_NAME: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        PACKAGE_NAME = getApplicationContext().getPackageName();
        Log.e("Hello",PACKAGE_NAME);
    }
}
